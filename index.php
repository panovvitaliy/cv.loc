<?php

// practicing with Git
// edit new brack
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/skills/SkillsRepository.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/education/EducationRepository.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/works/WorksRepository.php';

$skillRepository = new SkillsRepository();
$skills = $skillRepository->getAll();

$educationRepository = new EducationRepository();
$education = $educationRepository->getAll();

$worksRepository = new WorksRepository();
$works = $worksRepository->getAll();


$myName = 'Vitaliy Panov';
$myEmail = 'vitalik.od@gmail.com';
$myPhone = '+38 095 333 33 10';
$myCv = './cv_vp.pdf';
$myPhoto = 'assets/images/proflpic.png';
$myPosition = 'QA Engineer';
$aboutMe = "Hello everyone, My name is $myName. I have been working as a $myPosition almost three years. 
            I am really interested in this and want to grow in this sphere. I am very inspired and always open 
            to learning something new.<br>Hobbies: sports, traveling, activity.";


$menu = [
    "" => "Home",
    "about" => "About",
    "skills" => "Skills",
    "works" => "Works",
    "education" => "Education",
    "certificates" => "Certificates",
    "pricing-table" => "Pricing Table",
    "blog" => "Blog",
    "contact-form" => "Contact",
];


function diffTime($startDate, $endDate)
{
    $d1 = new DateTime($startDate);
    if (is_null($endDate)) {
        $d2 = new DateTime(date('Y-m-d'));
    } else {
        $d2 = new DateTime($endDate);
    }
    $diff = date_diff($d2, $d1);
    if ($diff->format('%m') < 1) {
        return null;
    } elseif ($diff->format('%y') < 1) {
        return $diff->format('%m months');
    } else {
        return $diff->format('%y years, %m months');
    }
}

$certificates = [
    [
        'url' => 'https://certificate.ithillel.ua/view/20860483/en',
        'subject' => 'QA Automation',
        'period' => 'December 2019',
        'coach' => 'Artur Pilyuk',
        'grade' => 'Satisfactory',
    ],
    [
        'url' => 'https://certificate.ithillel.ua/view/57387265/en',
        'subject' => 'Java for QA Automation',
        'period' => 'July 2019',
        'coach' => 'Artur Pilyuk',
        'grade' => 'Good',
    ],
    [
        'url' => 'https://certificate.ithillel.ua/view/86956675/en',
        'subject' => 'QA Manual',
        'period' => 'February 2018',
        'coach' => 'Robert Valek',
        'grade' => 'Very Good',
    ],
];

$blog = [
    [
        'title' => 'What is Quality Assurance',
        'image' => 'assets/images/b-1.jpg',
        'url' => 'blog-full-post.html',
        'tags' => 'testing, qa, article',
        'details' => 'Quality Assurance (QA) in simple words is a process of checking whether the
                            software meets some special requirements and functions as planned.',
    ],
    [
        'title' => 'Testing Your MicroService using Contract testing',
        'image' => 'assets/images/b-2.jpg',
        'url' => 'blog-full-post.html',
        'tags' => 'testing, qa, article',
        'details' => 'Contract testing is a way to ensure that services (such as an API provider and a
                            client) can communicate with each other. <br><br> Without contract testing, the
                            only way to know that services can communicate is by using expensive and brittle 
                            integration tests.',
    ],
    [
        'title' => 'What is, Types & Example',
        'image' => 'assets/images/b-3.jpg',
        'url' => 'blog-full-post.html',
        'tags' => 'testing, qa, article',
        'details' => 'Software Testing Metrics are the quantitative measures used to estimate the
                            progress, quality, productivity and health of the software testing process.',
    ],
];

$interests = [
    'facebook-official' => [
        'name' => 'Facebook',
        'url' => 'https://www.facebook.com/vitalik.panov.9',
    ],
    'linkedin' => [
        'name' => 'LinkedIn',
        'url' => 'https://www.linkedin.com/in/vitaliy-panov/',
    ],
    'envelope' => [
        'name' => 'Mail Me',
        'url' => "mailto:$myEmail",
    ],
];

$prices = [
    [
        'name' => 'START-UP',
        'price' => '$299',
        'list' => ['Logo', 'Domine & Hosting', 'One Page Landing', 'Email Marketing', 'SEO']
    ],
    [
        'name' => 'SMALL BUSINESS',
        'price' => '$499',
        'list' => ['Logo', 'Domine & Hosting', 'Adsence', 'One Page Landing', 'Email Marketing', 'SEO']
    ],
    [
        'name' => 'ENTERPRISE',
        'price' => '$799',
        'list' => ['Logo', 'Domine & Hosting', 'One Page Landing', 'One Page Landing', 'Email Marketing', 'SEO']
    ],
];

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<head>
    <meta charset="utf-8">

    <!-- TITLE OF SITE -->
    <title> <?php echo $myName ?> CV/Resume </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Materialize portfolio one page template, Using for personal website."/>
    <meta name="keywords" content="cv, resume, portfolio, materialize, onepage, personal, blog"/>
    <meta name="author" content="Siful Islam, DeviserWeb">

    <!-- FAVICON -->
    <link rel="icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">


    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:500,300,400' rel='stylesheet' type='text/css'>

    <!-- FRAMEWORK CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css">
    <!-- <link rel="stylesheet" href="css/lightbox.css"> -->

    <!-- FONT ICONS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


    <!-- ADDITIONAL CSS -->
    <link rel="stylesheet" href="assets/css/timeline.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css">

    <!--   COUSTOM CSS link  -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!--   COLOUR  -->
    <!-- <link rel="stylesheet" href="assets/css/color/lime.css" title="lime"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/red.css" title="red"> -->
    <link rel="stylesheet" href="assets/css/color/green.css" title="green"> <!-- Currently Usingthis color  -->
    <!-- <link rel="stylesheet" href="assets/css/color/purple.css" title="purple"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/orange.css" title="orange">  -->

</head>
<body>
<!-- =========================================
                   HEADER TOP
==========================================-->
<header id="header-top"> <!--Start: "Header Area"-->
    <div class="container">
        <div class="row">
            <div class="top-contact col m12 s12 right">
                <span><i class="fa fa-envelope"></i> <a href="mailto:"><?php echo $myEmail ?></a></span>
                <span><i class="fa fa-phone"></i> <a href="tel:"><?php echo $myPhone ?></a></span>
            </div>
        </div>
    </div>
    <!-- =========================================
                   NAVIGATION
    ==========================================-->
    <div id="header-bottom" class="z-depth-1"> <!--Start: "Header Area"-->
        <div id="sticky-nav">
            <div class="container">
                <div class="row">
                    <nav class="nav-wrap">
                        <ul class="hide-on-med-and-down group" id="example-one">
                            <?php foreach ($menu as $key => $name): ?>
                                <li class="current_page_item"><a href="#<?= $key ?>"><?= $name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <ul class="side-nav" id="slide-out">
                            <?php foreach ($menu as $key => $name): ?>
                                <li><a href="#<?= $key ?>" class="active"><?= $name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <a href="#" data-activates="slide-out" class="button-collapse"><i
                                    class="mdi-navigation-menu"></i></a>
                    </nav>
                </div>
            </div>
        </div>
    </div> <!--End: Header Area-->
</header> <!--End: Header Area-->

<!-- =========================================
                ABOUT
==========================================-->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="intro z-depth-1 col m12">
                <div class="col m12 s12">
                    <div class="profile-pic wow fadeIn a1" data-wow-delay="0.1s">
                        <img src="<?php echo $myPhoto ?>" alt="">
                    </div>
                </div>
                <div class="col m12 s12 co-centered wow fadeIn a2" data-wow-delay="0.2s">
                    <div class="intro-content">
                        <h2><?php echo $myName ?></h2>
                        <span><?php echo $myPosition ?> at XML Logistics</span>
                        <p><?php echo $aboutMe ?></p>
                        <a href="<?php echo $myCv ?>" download class="btn waves-effect">Download CV</a>
                        <a href="#contact-form" class="btn btn-success waves-effect">Contact Me</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- =========================================
                   SKILLS
==========================================-->
<section id="skills">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-gears"></i><?php echo $menu['skills'] ?></h2>
            </div>
            <div class="skill-line z-depth-1">
                <div class="row">
                    <div class="col m6 s12">
                        <div class="col m2 skill-icon">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                        <div class="skill-bar col m10 wow fadeIn a1" data-wow-delay="0.1s">
                            <h3>Professional Skills </h3>
                            <?php
                            foreach ($skills as $skill): if ($skill['type'] == 'pro'): ?>
                                <span><?= $skill['name'] ?></span>
                                <div class="progress">
                                    <div class="determinate"> <?= $skill['level'] ?>%</div>
                                </div>
                            <?php endif; endforeach; ?>
                        </div>
                    </div>
                    <div class="col m6 s12">
                        <div class="col m2 skill-icon">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                        <div class="skill-bar col m10 wow fadeIn a2" data-wow-delay="0.2s">
                            <h3>Personal Skills </h3>
                            <?php foreach ($skills as $skill): if ($skill['type'] == 'per'): ?>
                                <span><?= $skill['name'] ?></span>
                                <div class="progress">
                                    <div class="determinate"> <?= $skill['level'] ?>%</div>
                                </div>
                            <?php endif; endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ========================================
        WORKS EXPERIENCE
==========================================-->
<section id="works">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <h2><i class="fa fa-suitcase"> </i><?php echo $menu['works'] ?></h2>
            </div>
            <div id="cd-timeline" class="cd-container">
                <?php foreach ($works as $company => $data): ?>
                    <div class="cd-timeline-block wow fadeIn a<?= $company + 2 ?>>"
                         data-wow-delay="0.<?= $company + 2 ?>s">
                        <div class="cd-timeline-img">
                        </div> <!-- cd-timeline-img -->
                        <div class="cd-timeline-content col m5 s12 z-depth-1">
                            <a href="<?= $data['url'] ?>" target="_blank">
                                <h2><?= $myPosition . ' ' . $data['title'] ?></h2>
                            </a>
                            <span>
                                <?= $data['start_date'] . " — " ?>
                                <?php if (is_null($data['end_date'])) : echo "Present"; else: echo $data['end_date']; endif; ?>
                                <?= "<br>" . diffTime($data['start_date'], $data['end_date']) ?>
                            </span>
                            <p><?= $data['details'] ?></p>
                        </div> <!-- cd-timeline-content -->
                    </div> <!-- cd-timeline-block -->
                <?php endforeach; ?>
            </div> <!-- cd-timeline -->
        </div>
    </div>
</section>

<!-- =========================================
        EDUCATION
==========================================-->

<section id="education">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-graduation-cap"></i><?php echo $menu['education'] ?></h2>
            </div>
            <div class="cd-container" id="ed-timeline">
                <?php foreach ($education as $key => $data): ?>
                    <div class="cd-timeline-block wow fadeIn a<?= 2 + $key ?>" data-wow-delay="0.<?= 2 + $key ?>s">
                        <div class="cd-timeline-img"></div>
                        <div class="cd-timeline-content col m5 s12 z-depth-1">
                            <a href="<?= $data['url'] ?>" target="_blank">
                                <h2><?= $data['subject'] . ' ' . $data['school'] ?> </h2>
                            </a>
                            <span><?= $data['period'] ?> </span>
                            <p><?= $data['details'] ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div> <!-- cd-timeline -->
        </div>
    </div>
</section>


<!-- ========================================
       CERTIFICATES
==========================================-->
<section id="certificates">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <h2><i class="fa fa-certificate"> </i><?php echo $menu['certificates'] ?></h2>
            </div>
            <div id="cd-timeline" class="cd-container">
                <?php foreach ($certificates as $certificate => $data): ?>
                    <div class="cd-timeline-block wow fadeIn a<?= $certificate + 2 ?>"
                         data-wow-delay="0.<?= $certificate + 2 ?>s">
                        <div class="cd-timeline-img">
                        </div> <!-- cd-timeline-img -->
                        <div class="cd-timeline-content col m5 s12 z-depth-1">
                            <a href="<?= $data['url'] ?>" target="_blank">
                                <h2><?= $data['subject'] ?></h2></a>
                            <span><?= $data['period'] ?></span>
                            <p>Coach: <?= $data['coach'] ?> <br>
                                Grade: <?= $data['grade'] ?></p>
                        </div> <!-- cd-timeline-content -->
                    </div> <!-- cd-timeline-block -->
                <?php endforeach; ?>
            </div> <!-- cd-timeline -->
        </div>
    </div>
</section>


<!-- =========================================
          PRICING
 ==========================================-->
<section id="pricing-table">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa  fa-money "></i><?php echo $menu['pricing-table'] ?> </h2>
            </div>
            <div class="pricing wow fadeIn a3" data-wow-delay="0.3s">
                <div class="row">
                    <?php foreach ($prices as $price => $data): ?>
                        <div class="plan col m4 s12">
                            <ul class="thumbnail z-depth-1">
                                <li><strong><?= $data['name'] ?></strong></li>
                                <li><h3><?= $data['price'] ?></h3></li>
                                <?php for ($i = 0, $dataCount = count($data['list']); $i < $dataCount; $i++): ?>
                                    <li>
                                        <div class="span"> <?= $data['list'][$i] ?></div>
                                    </li>
                                <?php endfor; ?>
                                <li>
                                    <button type="button" class="btn btn-info waves-effect">Order now</button>
                                </li>
                            </ul>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================================
        BLOG
==========================================-->
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-edit"> </i><?php echo $menu['blog'] ?></h2>
            </div>
            <div class="row">
                <div class="blog">
                    <?php for ($i = 0, $dataCount = count($blog); $i < $dataCount; $i++): ?>
                        <div class="col m4 s12 blog-post wow fadeIn a<?= $i + 2 ?>" data-wow-delay="0.<?= $i + 2 ?>s">
                            <div class="thumbnail z-depth-1 animated">
                                <a href="<?= $blog[$i]['url'] ?>"><img
                                            src="<?= $blog[$i]['image'] ?>" alt=""
                                            class="responsive-img"></a>
                                <div class="blog-details">
                                    <div class="post-title" id="blog-post-<?= $i + 1 ?>">
                                        <a href="blog-full-post.html">
                                            <h2><?= $blog[$i]['title'] ?></h2>
                                            <span><?= $blog[$i]['tags'] ?></span>
                                        </a>
                                    </div>
                                    <div class="post-details">
                                        <p><?= $blog[$i]['details'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- =========================================
        CONTACT
==========================================-->
<section id="contact-form">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-send"></i><?php echo $menu['contact-form'] ?></h2>
            </div>
            <div class="contact-form z-depth-1" id="contact">
                <div class="row">
                    <form id="contactForm" d>
                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                        <div class="input-field col m6 s12 wow fadeIn a2" data-wow-delay="0.2s">
                            <label for="name" class="h4">Full Name *</label>
                            <input type="text" class="form-control validate" id="name" required
                                   data-error="NEW ERROR MESSAGE">
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a4" data-wow-delay="0.4s">
                            <label for="email" class="h4">Email *</label>
                            <input type="email" class="form-control validate" id="email" required>
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a3" data-wow-delay="0.3s">
                            <label for="last_name" class="h4">Subject *</label>
                            <input type="text" class="form-control validate" id="last_name" required>
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a5" data-wow-delay="0.5s">
                            <select>
                                <option value="">Choose your Budget</option>
                                <option value="1">$10000-$20000</option>
                                <option value="2">$50000-$100000</option>
                                <option value="3">$50000-$1000000</option>
                            </select>
                        </div>
                        <div class="input-field col m12 s12 wow fadeIn a6" data-wow-delay="0.6s">
                            <label for="message" class="h4 ">Message *</label>
                            <textarea id="message" class="form-control materialize-textarea validate"
                                      required></textarea>
                        </div>
                        <button type="submit" id="form-submit" class="btn btn-success waves-effect wow fadeIn a7"
                                data-wow-delay="0.7s">Submit
                        </button>

                    </form>
                </div>
            </div>

            <!-- =========================================
                    INTEREST
            ==========================================-->

            <div class="interests col s12 m12 l12 wow fadeIn" data-wow-delay="0.1s">
                <div class="row">
                    <ul class="z-depth-1"> <!-- interests icon start -->
                        <?php foreach ($interests as $resource => $data): ?>
                            <li><a href="<?= $data['url'] ?>"
                                   class="fa fa-<?= $resource ?> tooltipped col m2 s6" data-position="top"
                                   data-delay="50"
                                   data-tooltip="<?= $data['name'] ?>"></a></li>
                        <?php endforeach; ?>
                    </ul> <!-- interests icon end -->
                </div>
            </div>
        </div>
    </div>
</section>

<!-- SCRIPTS -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.95.3/js/materialize.min.js'></script>
<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
<script src="assets/js/masonry.pkgd.js"></script>
<script src="assets/js/jquery.fancybox.pack.js"></script>
<script src="assets/js/validator.min.js"></script>
<script src="assets/js/jquery.nav.js"></script>
<script src="assets/js/modernizr.js"></script>
<script src="assets/js/jquery.sticky.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/init.js"></script>


</body>
</html>