<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$array = [1, 2, 3, 4, 5, 6, 2, 7, 3, 8];

print_r($array);
echo "<hr>";

//-----------------------------------------------------
//              COUNT
//-----------------------------------------------------

function myCount(array $arr): int
{
    $quantity = 0;
    foreach ($arr as $value) $quantity++;
    return $quantity;
}

echo 'System function <b>count($array):</b> ' . count($array);
echo '</br>Custom function <b>myCount($array):</b> ' . myCount($array);
echo "<hr>";

//-----------------------------------------------------
//              IN_ARRAY
//-----------------------------------------------------

function myIn_array($find, array $arr): bool
{
    $result = false;
    foreach ($arr as $value) if ($value == $find) {
        $result = true;
    }
    return $result;
}

if (in_array(4, $array)) {
    echo 'System function <b>in_array(4, $array):</b> 4 exist in array';
}
if (myIn_array(4, $array)) {
    echo '</br>Custom function <b>myIn_array(4, $array):</b> 4 exist in array';
}

//-----------------------------------------------------
//              ARRAY_SEARCH
//-----------------------------------------------------

function myArray_search($find, array $arr): string
{
    $result = null;
    foreach ($arr as $key => $value) if ($value == $find) {
        $result = $key;
    }
    return $result;
}

echo "<hr>";
echo 'System function <b>array_search(4, $array):</b> ' . array_search(4, $array);
echo '</br>Custom function <b>myArray_search(4, $array):</b> ' . myArray_search(4, $array);

//-----------------------------------------------------
//              ARRAY_UNIQUE
//-----------------------------------------------------

function myArray_unique(array $arr): array
{
    $uniqueArr = array();
    foreach ($arr as $key => $value) if (!myIn_array($value, $uniqueArr)) {
        $uniqueArr[$key] = $value;
    }
    return $uniqueArr;
}

echo "<hr>";
echo 'Custom function <b>myArray_unique($array):</b> ';
print_r(myArray_unique($array));
echo '<br>System function <b>array_unique($array):</b> ';
print_r(array_unique($array));


//-----------------------------------------------------
//              ARRAY_FLIP
//-----------------------------------------------------

$baseArr = [
    0 => 'zero',
    '1' => 'one',
    'two' => 2,
    3 => 'three',
    4 => 4,
];

echo '<hr> <pre> Base Array:<br>';
print_r($baseArr);


function myArray_flip(array $arr): array
{
    $newArr = [];
    foreach ($arr as $key => $value) {
        $newArr[$value] = $key;
    }

    return $newArr;
}


echo 'Custom function <b>myArray_flip($baseArr):</b> <br>';
print_r(myArray_flip($baseArr));
echo '<br>System function <b>array_flip($baseArray):</b> <br>';
print_r(array_flip($baseArr));


//-----------------------------------------------------
//              ARRAY_COLUMN
//-----------------------------------------------------

$records = [
    [
        'id' => 2135,
        'first_name' => 'John',
        'last_name' => 'Doe',
    ],
    [
        'id' => 3245,
        'first_name' => 'Sally',
        'last_name' => 'Smith',
    ],
    [
        'id' => 5342,
        'first_name' => 'Jane',
        'last_name' => 'Jones',
    ],
    [
        'id' => 5623,
        'first_name' => 'Peter',
        'last_name' => 'Doe',
    ]
];

echo "<hr> \$records = ";
print_r($records);
function myArray_column(array $arr, $column_key, $index_key = null): array
{

    foreach ($arr as $data) {
        if ($index_key) {
            $newArr[$data[$index_key]] = $data[$column_key];
        } else {
            $newArr[] = $data[$column_key];
        }
    }
    return $newArr;
}


echo 'Custom function <b>myArray_column($records, \'first_name\', \'id\'):</b> <br>';
print_r(myArray_column($records, 'first_name', 'id'));

echo 'System function <b>array_column($records, \'first_name\', \'id\'):</b> <br>';
print_r(array_column($records, 'first_name', 'id'));


echo '</pre>';
