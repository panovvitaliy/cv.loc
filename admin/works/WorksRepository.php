<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/DBConnector.php';

/**
 * Class WorksRepository
 */
class WorksRepository
{

    /**
     * @var PDO|null
     */
    private $connect;

    /**
     * WorksRepository constructor.
     */
    public function __construct()
    {
        $this->connect = DBConnector::getInstance();
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $sql = "select * from cv.works ";
        $sth = $this->connect->prepare($sql);
        $sth->execute();

        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function getById(string $id)
    {
        $sql = "select * from cv.works where id = :id";
        $sth = $this->connect->prepare($sql);
        $sth->bindParam(':id', $id);
        $sth->execute();

        return $sth->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $params
     * @return bool
     */
    public function save(array $params): bool
    {
        $sql = <<< SQL
            INSERT INTO 
                works (company, position , startDate, endDate, description ) 
            VALUES 
                   (:company, :position , :startDate, :endDate, :description );
SQL;

        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $this->connect->prepare($sql);
        $stmt->bindParam(':company', $params['company']);
        $stmt->bindParam(':position', $params['position']);
        $stmt->bindParam(':startDate', $params['startDate']);
        $stmt->bindParam(':endDate', $params['endDate']);
        $stmt->bindParam(':description', $params['description']);

        return $stmt->execute();
    }

    /**
     * @param array $params
     * @return bool
     */
    public function update(array $params): bool
    {
        $sql = <<< SQL
            UPDATE 
                works
             SET company = :company,
              position=:position,
               startDate = :startDate,
               endDate = :endDate,
               description = :description,
            where id = :id
SQL;

        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $this->connect->prepare($sql);
        $stmt->bindParam(':id', $params['id']);
        $stmt->bindParam(':company', $params['company']);
        $stmt->bindParam(':position', $params['position']);
        $stmt->bindParam(':startDate', $params['startDate']);
        $stmt->bindParam(':endDate', $params['endDate']);
        $stmt->bindParam(':description', $params['description']);

        return $stmt->execute();
    }

    /**
     * @param string $id
     * @return bool
     */
    public function delete(string $id): bool
    {
        $sql = <<< SQL
            DELETE FROM
                works
            where id = :id
SQL;
        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $this->connect->prepare($sql);
        $stmt->bindParam(':id', $id);

        return $stmt->execute();
    }
}