<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/skills/SkillsRepository.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/skills/SkillsValidator.php';

$skillRepository = new SkillsRepository();
$skillValidator = new SkillsValidator();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($skillValidator->validate($_POST)) {
        $skillRepository->update($_POST);
        header('Location: /admin/skills/per');
    } else {
        $errors = $skillValidator->getErrors();
        header('Location: /admin/skills/per/formUpdate.php');
    }
}

