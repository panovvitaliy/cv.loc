<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/skills/SkillsRepository.php';

$skillRepository = new SkillsRepository();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $skillRepository->update($_POST);
    header('Location: /admin/skills/pro');
}

