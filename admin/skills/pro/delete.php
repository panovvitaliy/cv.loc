<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/skills/SkillsRepository.php';

$skillRepository = new SkillsRepository();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $skillRepository->delete($_GET['id']);
    header('Location: /admin/skills/pro');
}


